# Suivi de Projet

## 29 Janvier
### Tâches réalisées:
- Trouver quel jeu modéliser.
- Veille technologique : recherches pour savoir comment implémenter de la connexion de proximité. Étude du code de frozen bubble (f droid).
- Création et configuration d'un espace de travail, de gestion de projet (Jira, Confluence).

### Résultats:
- Découverte de l'API de Google : Nearby Connections. Recherche de documentation sur cette API qui semble convenir à nos besoins.
- Décision d'utiliser Android Studio et le langage Kotlin pour implémenter l'application et xml pour le visuel.

## 12 Février
### Tâches réalisées:
- Apprentissage de l'IDE Android Studio et du langage Kotlin.
- Développement du prototype de connexion entre 3 appareils à l'aide de l'API Nearby Connection.
- Création de documents de gestion de projet (Matrice SWOT, Cahier des charges, Gantt, personna…).

### Résultats:
- Découverte des difficultés de la simulation d'une connexion à 3 personnes, en effet l'émulateur intégré à l'IDE crash constamment sur un ordinateur pas assez puissant. Nous devons donc interconnecter nos téléphones.
- Une meilleure vision du projet vis-à-vis du périmètre du jeu et de l'implémentation.

## 4 Mars
### Tâches réalisées:
- Diagramme de classe.
- Diagramme de séquence de deux tours d'une partie de jeu.
- Création des visuels de l'ensemble des pages de notre jeu, ainsi que le lien entre les pages sur l'application figma.

### Résultats:
- 1ère version du diagramme de classe UML. Maquettes des différentes pages du jeu dessinées avec Figma.

## 11 Mars
### Tâches réalisées:
- Initialisation du projet et de son squelette architectural.
- Corriger et améliorer le diagramme de classe, discussion sur l'architecture du jeu.
- Codage du visuel des premières pages de l'application (pseudo_choice, home_page), aucune interaction pour le moment.

### Résultats:
- Élaboration d'un diagramme de séquence pour 2 tours de jeu.
- Les visuels sont fonctionnels sur l'émulateur d'android studio et sur un téléphone physique.

## 12 Mars
### Tâches réalisées:
- Création des pages importantes en xml.
- Création globale des ViewModel pour l'host et le client et établissement d'une connexion entre les deux à partir du code source d'un jeu de morpion trouvé sur f droid.

### Résultats:
- Système de traitement de payloads robuste à l'aide d'un JSON.
- Le visuel de l'ensemble des pages de l'application est fonctionnel.

## 14 Mars
### Tâches réalisées:
- Répertorier les différentes interactions entre host et client (Payload). En déduire le format des requêtes (données,etc…) et leur traitement.
- Implémentation de premiers liens entre des pages.

### Résultats:
- Les 4 premières pages de l'application sont liées. On peut désormais naviguer entre les premières pages de notre application.

## 18 Mars
### Tâches réalisées:
- Ajouter les classes pour les différents objets de notre jeu (Cartes, Plateau de jeu, Partie…).
- Ajouter la navigation vers de nouvelles pages.
- Nettoyage de la classe MainActivity.

### Résultats:
- Les pages de bet sont désormais accessibles.
- La classe MainActivity ne contient plus les codes liés à la connexion entre device.

## 19 Mars
### Tâches réalisées:
- Ajouter et commencer à implémenter les ViewModel.

### Résultats:
- Les ViewModel ont bien été ajoutés, mais on s'est rendu compte que les ViewModel ne sont pas partageables entre les Activités. Nous avons donc modifié les activités en fragment.

## 22 Mars
### Tâches réalisées:
- Implémentation des premiers listeners et des premiers envois et receptions de payloads (pseudos des joueurs). Ajout des assets pour les cartes.

### Résultats:
- La liste des joueurs dans le salon d'attente est mise à jour à chaque fois qu'un joueur rejoint le salon.

## 25 Mars
### Tâches réalisées:
- Poursuivre les tâches du 22 Mars.
- Ajouter

 des contraintes d'interaction sur les pages pour distinguer client et host.
- Creer la page du plateau de jeu.
- Correction des bugs côté serveur et ajout de nouvelles fonctionnalités d'interactions client/serveur.

### Résultats:
- Première version du plateau de jeu, sans les cartes.
- Seulement l'Host peut cliquer sur le bouton lancer le choix des paris.

## 26 Mars
### Tâches réalisées:
- Compléter le plateau de jeu et ajouter des interactions.
- Ajouter un chemin vers le plateau de jeu.
- Ajouter les nouvelles interactions avec le plateau de jeu du côté du serveur.

### Résultats:
- Le plateau est complet et est accessible depuis la page Teams_pages.
- On peut désormais tirer une carte qui s'affiche correctement.

## 29 Mars
### Tâches réalisées:
- Ajouter de la robustesse réseau à notre implémentation.
- Construire les popups de la partie.
- Ajouter le mouvement des cartes.
- Ajouter l'automate de la gestion de la partie côté serveur.

### Résultats:
- Un client peut désormais se déconnecter du lobby sans faire planter les applications.

## 02 Avril
### Tâches réalisées:
- Corriger les bugs de l'automate.
- Corriger l'accessibilité du bouton drawCard après un vote.
- Ajouter une gestion de la fin de partie.

### Résultats:
- Automate totalement fonctionnel.
- Les interactions de fin de partie ont été implémentés côté serveur.

## 05 Avril
### Tâches réalisées:
- Terminer la gestion de fin de partie.
- Corriger les bugs majeurs.

### Résultats:
- Le jeu est jouable de bout-en-bout.
- Il reste un bug, on est obligé de fermer l'application pour pouvoir lancer une nouvelle partie.
